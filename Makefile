ifndef VERBOSE
.SILENT:
endif

CC=clang
CFLAGS=$(shell pkg-config --cflags libadwaita-1 json-c sqlite3) -lcurl
LIBS=$(shell pkg-config --libs libadwaita-1 json-c sqlite3)
FLAGS=-Wall -g
C_FILES=src/*.c src/ui/*.c src/api/*.c src/json/*.c src/database/*.c
OUTPUT=moonlit

compile:
	$(CC) $(FLAGS) $(CFLAGS) $(LIBS) $(C_FILES) -o $(OUTPUT)
execute:
	$(OUTPUT)
clean:
	rm $(OUTPUT)
install:compile
	chmod +x moonlit.sh
	sudo mkdir -p /opt/moonlit
	sudo cp moonlit.sh /usr/bin/moonlit
	sudo cp moonlit /opt/moonlit
	sudo mkdir -p /opt/moonlit/ui
	sudo cp -r ui/*.ui ui/*.css /opt/moonlit/ui
	sudo cp -r ui/icons/* /usr/share/icons/hicolor
	sudo gtk-update-icon-cache -f /usr/share/icons/hicolor
run:install execute clean
