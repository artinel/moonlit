#include<libadwaita-1/adwaita.h>
#include<stdlib.h>
#include"ui/main_window.h"
#include"structs.h"
#include"api/api.h"
#include"database/db.h"

#define APPLICATION_PACKAGE "com.moonlit.client"

void app_init();
void __attribute__((constructor)) app_init();
void __attribute__((destructor)) app_exit();

int main(int argc, char** argv){

	AdwApplication* app = adw_application_new(APPLICATION_PACKAGE, G_APPLICATION_DEFAULT_FLAGS);
	
	g_signal_connect(app, "activate", G_CALLBACK(main_window_activate), NULL);

	return g_application_run(G_APPLICATION(app), argc, argv);
}

void app_init(){
	//Init curl
	api_init();
	//Create Directories if not exists
	system("mkdir -p ~/.local/share/moonlit");
	//Init database
	if(db_init() == DB_FAIL)
		exit(1);
}

void app_exit(){
	api_exit();
}
