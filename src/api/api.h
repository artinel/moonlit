typedef enum{
	API_CODE_OK,
	API_CODE_NOT_FOUND,
	API_CODE_EXISTS,
	API_CODE_FAIL
}api_code;

typedef struct{
	int code;
	char* response;
}api_result;

void api_init();
void api_exit();

void api_login(char* username, char* password, void (*callback)(api_result));
void api_register(char*username, char* password, char* firstname, char*lastname, void(*callback)(api_result));
