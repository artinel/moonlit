#include<libadwaita-1/adwaita.h>
#include<curl/curl.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"../structs.h"
#include"api.h"
#include"../json/json.h"

#define API_BASE "http://localhost/rio/api/"
#define API_LOGIN "login"
#define API_REGISTER "register"

struct memory{
	char* memory;
	size_t size;
};

typedef struct{
	char* api;
	char* params;
	void (*callback)(api_result);
}api_request_data;

static GThread* thread;
static api_result result;

static char* make_url(char* api){
	char* url = malloc(strlen(API_BASE) + strlen(api) + 1);
	sprintf(url, "%s%s", API_BASE, api);
	return url;
}



static size_t write_memory(void* content, size_t size, size_t nmemb, void* userp){
	size_t real_size = size * nmemb;
	struct memory *mem = (struct memory*) userp;

	char* ptr = realloc(mem->memory,mem->size + real_size + 1);
	if(!ptr){
		printf("Not enough memory\n");
		return 0;
	}

	mem->memory = ptr;
	memcpy(&(mem->memory[mem->size]), content, real_size);
	mem->size += real_size;
	mem->memory[mem->size] = 0;
	return real_size;
}

void api_init(){
	curl_global_init(CURL_GLOBAL_ALL);
}

void api_exit(){
	curl_global_cleanup();
}

static void api_request(gpointer p){

	api_request_data* data = (api_request_data*) p;

	CURL* curl;
	CURLcode res = -1;
	struct memory chunk;

	chunk.memory = malloc(1);
	chunk.size = 0;

	curl = curl_easy_init();
	if(curl){
		char* url = make_url(data->api);
		curl_easy_setopt(curl, CURLOPT_URL, url);
		curl_easy_setopt(curl, CURLOPT_POSTFIELDS, data->params);
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_memory);
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void*)&chunk);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");

		res = curl_easy_perform(curl);
		if(res != CURLE_OK){
			fprintf(stderr, "Failed : %s\n", curl_easy_strerror(res));
		}
		curl_easy_cleanup(curl);
		free(url);
	}
	result.code = (int)res;
	result.response = malloc(strlen(chunk.memory) + 1);
	strcpy(result.response, chunk.memory);
	free(chunk.memory);
	data->callback(result);
	g_thread_exit(thread);
	thread = NULL;
	free(data->params);
	free(data);
}

void api_login(char* username, char* password, void (*callback)(api_result)){

	char* fields = malloc(strlen(username) + strlen(password) + strlen("username=&password=") + 1);
	sprintf(fields, "username=%s&password=%s", username, password);
	
	api_request_data* data = malloc(sizeof(api_request_data));
	data->api = API_LOGIN;
	data->params = fields;
	data->callback = callback;

	thread = g_thread_new("login-thread", (GThreadFunc)api_request, data);
}

void api_register(char* username, char* password, char* firstname, char* lastname, void (*callback)(api_result)){
	char* fields = malloc(strlen(username) + strlen(password) + strlen(firstname) + strlen(lastname) + strlen("username=&password=&firstname=&lastname=") + 1);
	sprintf(fields, "username=%s&password=%s&firstname=%s&lastname=%s", username, password, firstname, lastname);

	api_request_data* data = malloc(sizeof(api_request_data));
	data->api = API_REGISTER;
	data->params = fields;
	data->callback = callback;

	thread = g_thread_new("register-thread", (GThreadFunc)api_request, data);
}




