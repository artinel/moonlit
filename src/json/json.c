#include<json-c/json.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"../structs.h"
#include"json.h"

struct_login json_login(char* json){
	json_object* root = json_tokener_parse(json);
	json_object* code = json_object_object_get(root, "code");
	json_object* token = json_object_object_get(root, "token");

	struct_login data;
	data.code = json_object_get_int(code);
	data.token = (char*)json_object_get_string(token);

	return data;
}
