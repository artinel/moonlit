#define DB_PATH "/.local/share/moonlit/data.db"

typedef enum{
	DB_SUCCESS,
	DB_FAIL,
	DB_CLOSE,
	DB_OPEN,
	DB_EXISTS,
	DB_NOT_EXISTS
}db_signal;

db_signal db_init();

//Table ===> token
#define DB_TOKEN "token"
#define DB_TOKEN_TOKEN "token"

db_signal db_token_new(char* token);
db_signal db_token_clear();
char* db_token_get();
