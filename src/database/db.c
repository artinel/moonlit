#include<sqlite3.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"db.h"

static sqlite3* db;
static db_signal db_state = DB_CLOSE;

static void db_err(){
	const char* err = sqlite3_errmsg(db);
	const int code = sqlite3_errcode(db);
	if(err != NULL && code != 0 && code != 101 && code != 100){
		fprintf(stderr, "Database Error(%d) : %s\n", code, err);
	}
}

static db_signal db_open(){
	if(db_state == DB_CLOSE){
		char* dir = malloc(strlen(getenv("HOME")) + strlen(DB_PATH) + 1);
		sprintf(dir, "%s%s", getenv("HOME"), DB_PATH);
		if(sqlite3_open(dir, &db) == DB_SUCCESS){
			free(dir);
			db_state = DB_OPEN;
			return DB_SUCCESS;
		}else{
			free(dir);
			db_err();
			return DB_FAIL;
		}
	}
	return DB_SUCCESS;
}

static db_signal db_close(){
	if(db_state == DB_OPEN){
		if(sqlite3_close(db) == DB_SUCCESS){
			db_state = DB_CLOSE;
			return DB_SUCCESS;
		}else{
			db_err();
			return DB_FAIL;
		}
	}
	return DB_SUCCESS;
}

static char* db_sql(char* base, ...){
	va_list data;
	va_start(data, base);
	char* sql = malloc(strlen(base) + sizeof(data) + 1);
	vsprintf(sql, base, data);
	va_end(data);
	return sql;
}

db_signal db_init(){
	if(db_open() == DB_SUCCESS){
		int sql_count = 1;
		char* sqls[sql_count];

		char* token_sql = db_sql("CREATE TABLE IF NOT EXISTS %s(%s TEXT);", DB_TOKEN, DB_TOKEN_TOKEN);

		sqls[0] = token_sql;

		sqlite3_stmt* stmt;
		for(int i = 0; i < sql_count; i++){
			sqlite3_prepare_v2(db, sqls[i], -1, &stmt, 0);
			int status = sqlite3_step(stmt);
			db_err();
			sqlite3_finalize(stmt);
			if(status != SQLITE_OK && status != 101)
				return DB_FAIL;
		}
		free(token_sql);
		return DB_SUCCESS;
	}
	return DB_FAIL;
}

db_signal db_token_clear(){
	if(db_open() == DB_SUCCESS){
		char* sql = db_sql("DELETE  FROM %s;", DB_TOKEN);
		sqlite3_stmt* stmt;
		sqlite3_prepare_v2(db, sql, -1, &stmt, 0);
		int status = sqlite3_step(stmt);
		db_err();
		free(sql);
		sqlite3_finalize(stmt);
		if(status == SQLITE_DONE)
			return DB_SUCCESS;
	}
	return DB_FAIL;
}

db_signal db_token_new(char* token){
	if(db_token_clear() == DB_SUCCESS && db_open() == DB_SUCCESS){
		char* sql = db_sql("INSERT INTO %s VALUES(?);", DB_TOKEN);
		sqlite3_stmt* stmt;
		sqlite3_prepare_v2(db, sql, -1, &stmt, 0);
		sqlite3_bind_text(stmt, 1, token, -1, SQLITE_TRANSIENT);
		int status = sqlite3_step(stmt);
		free(sql);
		sqlite3_finalize(stmt);
		if(status == SQLITE_OK)
			return DB_SUCCESS;
	}
	return DB_FAIL;
}

char* db_token_get(){
	if(db_open() == DB_SUCCESS){
		char* sql = db_sql("SELECT * FROM %s;", DB_TOKEN);
		sqlite3_stmt* stmt;
		sqlite3_prepare_v2(db, sql, -1, &stmt, 0);
		int status = sqlite3_step(stmt);
		db_err();
		char* res = malloc(52);
		if(status == SQLITE_DONE || status == 100){
			sprintf(res, "%s", sqlite3_column_text(stmt, 0));
		}else{
			res = NULL;
		}
		sqlite3_finalize(stmt);
		free(sql);
		return res;
	}
	return NULL;
}
