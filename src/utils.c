#include<libadwaita-1/adwaita.h>
#include<stdio.h>
#include"utils.h"

GtkBuilder* load_ui(const char* ui){
	GtkBuilder* builder = gtk_builder_new();
	GError* err = NULL;
	gtk_builder_add_from_file(builder, ui, &err);
	if(err != NULL){
		printf("Error in loading ui file(%s) : %s\n", ui, err->message);
		return NULL;
	}

	return builder;
}

GtkWidget* get_object(GtkBuilder* builder, const char* id){
	return GTK_WIDGET(gtk_builder_get_object(builder, id));
}

void load_css(GtkWidget* window, const char* css){
	GtkCssProvider* provider = gtk_css_provider_new();
	gtk_css_provider_load_from_path(provider, css);
	gtk_style_context_add_provider_for_display(gtk_widget_get_display(window), 
			GTK_STYLE_PROVIDER(provider),
			GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

char* entry_get_text(GtkWidget* entry){
	return (char*)gtk_editable_get_text(GTK_EDITABLE(entry));
}

void widget_error(GtkWidget* widget){
	gtk_widget_add_css_class(widget, "error");
}

void widget_errorno(GtkWidget* widget){
	gtk_widget_remove_css_class(widget, "error");
}

void show_alert_dialog(char* title, char* message, GtkWidget* parent){
	AdwDialog* alert_dialog = adw_alert_dialog_new(title, NULL);
	adw_alert_dialog_format_body(ADW_ALERT_DIALOG(alert_dialog), "%s", message);
	adw_alert_dialog_add_response(ADW_ALERT_DIALOG(alert_dialog), "close", "Close");
	adw_alert_dialog_set_close_response(ADW_ALERT_DIALOG(alert_dialog), "close");
	adw_alert_dialog_set_response_appearance(ADW_ALERT_DIALOG(alert_dialog), "close", ADW_RESPONSE_SUGGESTED);
	adw_dialog_present(alert_dialog, parent);
}

struct entry_counter_data{
	int min;
	int max;
	GtkWidget* label;
	GtkWidget* self;
};

static void entry_counter_event(GtkWidget* self, gpointer user_data){
	struct entry_counter_data* data = (struct entry_counter_data*)user_data;

	int length = strlen(entry_get_text(self));

	char* text = malloc(10);
	sprintf(text, "%d/%d", length, data->max);

	gtk_label_set_label(GTK_LABEL(data->label), text);

	if(length < data->min || length > data->max)
		widget_error(data->label);
	else{
		widget_errorno(data->label);
		widget_errorno(data->self);
	}

	free(text);
}

void entry_add_counter(GtkWidget* entry, int min, int max){
	char* text = malloc(7);
	sprintf(text, "0/%d", max);
	GtkWidget* label = gtk_label_new(text);
	adw_entry_row_add_suffix(ADW_ENTRY_ROW(entry), label);

	if(min > 0)
		widget_error(label);

	struct entry_counter_data* data = malloc(sizeof(struct entry_counter_data));
	data->min = min;
	data->max = max;
	data->label = label;
	data->self = entry;

	g_signal_connect(gtk_editable_get_delegate(GTK_EDITABLE(entry)), "changed", G_CALLBACK(entry_counter_event), data);

	free(text);
}

void set_loading(GtkWidget* view){
	GtkBuilder* builder = load_ui("ui/loading_page.ui");
	GtkWidget* loading = get_object(builder, "loading");
	adw_toolbar_view_set_content(ADW_TOOLBAR_VIEW(view), loading);
}

static void set_error_page(GtkWidget* view, char* icon, char* title, char* desc, char* btn_text, void (*func)(void)){
	GtkWidget* box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 20);
	gtk_widget_set_valign(box, GTK_ALIGN_CENTER);
	gtk_widget_set_halign(box, GTK_ALIGN_FILL);

	GtkWidget* status = adw_status_page_new();
	adw_status_page_set_icon_name(ADW_STATUS_PAGE(status), icon);
	adw_status_page_set_title(ADW_STATUS_PAGE(status), title);
	adw_status_page_set_description(ADW_STATUS_PAGE(status), desc);

	if(func != NULL){
		GtkWidget* btn = gtk_button_new_with_label(btn_text);
		gtk_widget_add_css_class(btn, "suggested-action");
		gtk_widget_add_css_class(btn, "pill");
		gtk_widget_set_halign(btn, GTK_ALIGN_CENTER);
		g_signal_connect(btn, "clicked", G_CALLBACK(func), NULL);
		adw_status_page_set_child(ADW_STATUS_PAGE(status), btn);
	}
	gtk_box_append(GTK_BOX(box), status);
	adw_toolbar_view_set_content(ADW_TOOLBAR_VIEW(view), box);
}

void set_network_error(GtkWidget* view, void (*callback)(void)){
	set_error_page(view, "moonlit-network-error-symbolic", "Network Error", "Please check your internet connection and try again", "Retry", callback);
}
