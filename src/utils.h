GtkBuilder* load_ui(const char* ui);

GtkWidget* get_object(GtkBuilder* builder, const char* id);

void load_css(GtkWidget* window, const char* css);

void load_icon_theme();

char* entry_get_text(GtkWidget* entry);

void widget_error(GtkWidget* widget);

void widget_errorno(GtkWidget* widget);

void show_alert_dialog(char* title, char* message, GtkWidget* parent);

void entry_add_counter(GtkWidget* entry, int min, int max);

void set_loading(GtkWidget* view);

void set_network_error(GtkWidget* view, void (*callback)(void));
