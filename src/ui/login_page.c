#include<libadwaita-1/adwaita.h>
#include<string.h>
#include"../structs.h"
#include"../utils.h"
#include"../api/api.h"
#include"../json/json.h"
#include"login_page.h"
#include"../database/db.h"

#define USERNAME_MIN 5
#define USERNAME_MAX 20
#define PASSWORD_MIN 8
#define PASSWORD_MAX 20
#define FNAME_MIN 3
#define FNAME_MAX 30

static GtkWidget* view, *page_login, *page_register;
static GtkWidget* entry_login_username, *entry_login_password;
static GtkWidget* entry_register_username, *entry_register_password, *entry_register_firstname, *entry_register_lastname;
static GtkWidget* parent;

static void switch_to_register();
static void switch_to_login();
static void login();
static void _register();
static void login_callback(api_result);
static void register_callback(api_result);

GtkWidget* login_page_init(GtkWidget* p){
	
	parent = p;

	GtkBuilder* builder = load_ui("ui/login_page.ui");

	GtkWidget* page = get_object(builder, "page");

	view = get_object(builder, "view");

	char* token = db_token_get();
	if(token != NULL){

		page_login = get_object(builder, "page_login");
		page_register = get_object(builder, "page_register");

		GtkWidget* btn_switch_register = get_object(builder, "switch_register");
		GtkWidget* btn_switch_login = get_object(builder, "switch_login");

		entry_login_username = get_object(builder, "login_username");
		entry_login_password = get_object(builder, "login_password");
		GtkWidget* btn_login = get_object(builder, "login");

		entry_register_username = get_object(builder, "register_username");
		entry_register_password = get_object(builder, "register_password");
		entry_register_firstname = get_object(builder, "register_firstname");
		entry_register_lastname = get_object(builder, "register_lastname");
		GtkWidget* btn_register = get_object(builder, "register");

		entry_add_counter(entry_login_username, USERNAME_MIN, USERNAME_MAX);
		entry_add_counter(entry_login_password, PASSWORD_MIN, PASSWORD_MAX);

		entry_add_counter(entry_register_username, USERNAME_MIN, USERNAME_MAX);
		entry_add_counter(entry_register_password, PASSWORD_MIN, PASSWORD_MAX);
		entry_add_counter(entry_register_firstname, FNAME_MIN, FNAME_MAX);
		entry_add_counter(entry_register_lastname, FNAME_MIN, FNAME_MAX);

		adw_toolbar_view_set_content(ADW_TOOLBAR_VIEW(view), page_login);

		g_signal_connect(btn_switch_register, "clicked", G_CALLBACK(switch_to_register), NULL);
		g_signal_connect(btn_switch_login, "clicked", G_CALLBACK(switch_to_login), NULL);
		g_signal_connect(btn_login, "clicked", G_CALLBACK(login), NULL);
		g_signal_connect(btn_register, "clicked", G_CALLBACK(_register), NULL);
	}else{
	}
	return page;
}

static void switch_to_register(){
	adw_toolbar_view_set_content(ADW_TOOLBAR_VIEW(view), page_register);
}
static void switch_to_login(){
	adw_toolbar_view_set_content(ADW_TOOLBAR_VIEW(view), page_login);
}
static void login(){
	char* username = entry_get_text(entry_login_username);
	char* password = entry_get_text(entry_login_password);
	if(strlen(username) >= USERNAME_MIN && strlen(username) <= USERNAME_MAX){
		if(strlen(password) >= PASSWORD_MIN && strlen(password) <= PASSWORD_MAX){
			set_loading(view);
			api_login(username, password, login_callback);
		}else{
			widget_error(entry_login_password);
		}
	}else{
		widget_error(entry_login_username);
	}
}

static void login_callback(api_result result){
	adw_toolbar_view_set_content(ADW_TOOLBAR_VIEW(view), page_login);
	if(result.code == 0){
		struct_login data = json_login(result.response);
		if(data.code == API_CODE_OK){
			db_token_new(data.token);
		}else{
			show_alert_dialog("Error", "Username/Password is wrong", parent);
		}
	}else{
		set_network_error(view, login);
	}
}

static void _register(){
	char* username = entry_get_text(entry_register_username);
	char* password = entry_get_text(entry_register_password);
	char* firstname = entry_get_text(entry_register_firstname);
	char* lastname = entry_get_text(entry_register_lastname);
	
	if(strlen(username) >= USERNAME_MIN && strlen(username) <= USERNAME_MAX){
		if(strlen(password) >= PASSWORD_MIN && strlen(password) <= PASSWORD_MAX){
			if(strlen(firstname) >= FNAME_MIN && strlen(firstname) <= FNAME_MAX){
				if(strlen(lastname) >= FNAME_MIN && strlen(lastname) <= FNAME_MAX){
					set_loading(view);
					api_register(username, password, firstname, lastname, register_callback);
				}else{
					widget_error(entry_register_lastname);
				}
			}else{
				widget_error(entry_register_firstname);
			}
		}else{
			widget_error(entry_register_password);
		}
	}else{
		widget_error(entry_register_username);
	}
}

static void register_callback(api_result result){
	adw_toolbar_view_set_content(ADW_TOOLBAR_VIEW(view), page_register);
	if(result.code == 0){
		struct_login data = json_login(result.response);
		if(data.code == API_CODE_OK){
			db_token_new(data.token);
		}else if(data.code == API_CODE_EXISTS){
			show_alert_dialog("Error", "User already exists", parent);
		}else{
			show_alert_dialog("Failed", "Failed to register user!!!\nPlease try again", parent);
		}
	}else{
		set_network_error(view, _register);
	}
}
