#include<libadwaita-1/adwaita.h>
#include"main_window.h"
#include"../utils.h"
#include"login_page.h"
#include"../database/db.h"

#define SIDEBAR_ITEMS 1

static const char* sidebar_titles[SIDEBAR_ITEMS] = {"User Account"};
static const char* sidebar_icons[SIDEBAR_ITEMS] = {"moonlit-user-account-symbolic"};
static GtkWidget* (*sidebar_inits[SIDEBAR_ITEMS])(GtkWidget*) = {login_page_init};

static GtkWidget* sidebar_item_active;
static GtkWidget* main_page;
static GtkWidget* window;

static void add_sidebar_items(GtkWidget* sidebar);
static void sidebar_item_click(GtkWidget* self, gpointer data);

void main_window_activate(AdwApplication* app, gpointer user_data){
	GtkBuilder* builder = load_ui("ui/main_window.ui");
	window = get_object(builder, "window");
	main_page = get_object(builder, "main_page");

	gtk_window_set_application(GTK_WINDOW(window), GTK_APPLICATION(app));

	load_css(window, "ui/style.css");

	add_sidebar_items(get_object(builder, "sidebar"));
	
	gtk_window_present(GTK_WINDOW(window));
}

static void add_sidebar_items(GtkWidget* sidebar){
	for(int i = 0; i < SIDEBAR_ITEMS; i++){
		GtkWidget* button = gtk_button_new();
		GtkWidget* content = adw_button_content_new();
		adw_button_content_set_label(ADW_BUTTON_CONTENT(content), sidebar_titles[i]);
		adw_button_content_set_icon_name(ADW_BUTTON_CONTENT(content), sidebar_icons[i]);

		gtk_widget_set_halign(content, GTK_ALIGN_START);

		gtk_widget_add_css_class(content, "_sidebar-item");

		gtk_button_set_child(GTK_BUTTON(button), content);

		gtk_box_append(GTK_BOX(sidebar), button);
		
		if(i == 0){
			sidebar_item_active = button;
			gtk_widget_add_css_class(sidebar_item_active, "suggested-action");
			adw_navigation_split_view_set_content(ADW_NAVIGATION_SPLIT_VIEW(main_page), ADW_NAVIGATION_PAGE(sidebar_inits[i](window)));
		}

		g_signal_connect(button, "clicked", G_CALLBACK(sidebar_item_click), GINT_TO_POINTER(i));
	}
}

static void sidebar_item_click(GtkWidget* self, gpointer data){
	int index = GPOINTER_TO_INT(data);
	if(self != sidebar_item_active){
		gtk_widget_add_css_class(self, "suggested-action");
		gtk_widget_remove_css_class(sidebar_item_active, "suggested-action");
		sidebar_item_active = self;
		adw_navigation_split_view_set_content(ADW_NAVIGATION_SPLIT_VIEW(main_page), ADW_NAVIGATION_PAGE(sidebar_inits[index](window)));
	}
}
